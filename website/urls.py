from django.conf import settings
from django.conf.urls.static import static
from django.urls import path,include
from . import views
urlpatterns = [
    path('', views.homepage, name ='homepage'),
    path('logout/',views.logout_user, name ='logout'),
    path('register/',views.register_user, name ='register'),
    path('images/',views.image_merge, name ='images'),
    path('record/<int:pk>',views.customer_user, name ='record'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

