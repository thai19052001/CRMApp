from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import SignUpForm
from .models import Recorder
# from joblib import load
# Create your views here.


def homepage(request):
   records = Recorder.objects.all()
   if request.method == 'POST':
      username = request.POST['username']
      password = request.POST['password']
      user = authenticate(request,username = username, password = password)
      if user is not None:
         login(request,user)
         messages.success(request, "You have been logged in")
         return redirect('homepage')
      else:
         messages.error(request,"There was an error")
         return redirect('homepage')
   else:
      return render(request,'home.html',{'records' : records})


def logout_user(request):
   logout(request)
   messages.success(request, "You have been logged out")
   return redirect('homepage')


def register_user(request):
   if request.method == 'POST':
      form = SignUpForm(request.POST)
      if form.is_valid():
         form.save()
         username = form.cleaned_data['username']
         password = form.cleaned_data['password1']
         user = authenticate(username = username, password = password)
         login(request, user)
         messages.success(request, 'User registration successful')
         return redirect('homepage')
   else:
      form = SignUpForm()
      return render(request, 'register.html',{'form' : form})

def customer_user(request, pk):
   if request.user.is_authenticated:
      customer_record = Recorder.objects.get(id=pk)
      return render(request, 'record.html', {customer_record: customer_record})
   else:
      messages.success(request,'You must login to have information')
      return redirect('homepage')

def image_merge(request):
   # content_image = request.GET['content_image']
   # style_image = request.GET['style_image']
   
   return render(request, 'image.html', {})